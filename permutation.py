class permutation:
    def __init__(self, mapping=None):
        if mapping:
            if set(mapping) != set(range(max(mapping)+1)):
                raise Exception('invalid mapping')
        else:
            mapping = []
        self.mapping = mapping
        self.__cycles = None
        self.__period = None
    
    @classmethod
    def from_cycles(cls, cycles):
        mapping = list(range(max(sum(cycles,[]))+1))
        for cur in cycles:
            for i in range(len(cur)-1):
                x, y = cur[i], cur[i-1]
                mapping[x], mapping[y] = mapping[y], mapping[x]
        return cls(mapping)
    
    @property
    def cycles(self):
        if self.__cycles is not None: return self.__cycles
        
        self.__cycles = [[]]
        rem = set(self.mapping)
        while rem:
            cur = self.__cycles[-1]
            if cur:
                x = self.at(cur[-1])
                if x == cur[0]:
                    self.__cycles.append([])
                else:
                    cur.append(x)
                    rem.remove(x)
            else:
                cur.append(rem.pop())
        self.__cycles = [c for c in self.__cycles if len(c)>1]
        return self.__cycles
    
    @property
    def period(self):
        if self.__period is not None: return self.__period
        
        def gcd(a, b):
            return a if not b else gcd(b, a%b)
        
        self.__period = 1
        for l in self.cycles:
            l = len(l)
            self.__period = self.__period * l // gcd(self.__period, l)
        
        return self.__period
    
    def __mul__(self, x):
        return permutation(self[x.mapping])
    
    def __pow__(self, n):
        res = permutation()
        if abs(n) > 5:
            for d in bin(abs(n))[2:]:
                res *= res
                if d == '1': res *= self
        else:
            for _ in range(abs(n)): res *= self
        
        return res if n >= 0 else ~res
    
    def __invert__(self):
        return permutation([self.inv_at(i) for i in sorted(self.mapping)])
    
    def __str__(self):
        return '('+' '.join(str(i) for i in self.mapping)+')'
    
    def __getitem__(self, a):
        if isinstance(a, list):
            return [self.at(i) for i in a]
        else:
            return self.at(a)
    
    def at(self, a):
        return self.mapping[a] if a < len(self.mapping) else a
    
    def inv_at(self, a):
        return self.mapping.index(a) if a in self.mapping else a
    
