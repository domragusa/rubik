from permutation import permutation

class cube:
    def __init__(self):
        self.state = permutation()
        self.__faces = None
    
    @property
    def faces(self):
        if self.__faces is not None: return self.__faces
        
        self.__faces = []
        for off in range(0, 54, 9):
            self.__faces.append([])
            for i in range(off, off+9, 3):
                self.__faces[-1].append([
                    self.state[j]//9 for j in range(i, i+3)
                ])
        return self.__faces
    
    def rotate(self, m):
        if m not in cube.moves:
            raise Exception("unsupported rotation")
        self.state *= cube.moves[m]
        self.__faces = None
    
    # row number for each face
    #   0
    # 1 2 3 4
    #   5 
    
    rot_y = permutation([
         6,  3,  0,  7,  4,  1,  8,  5,  2,
        18, 19, 20, 21, 22, 23, 24, 25, 26,
        27, 28, 29, 30, 31, 32, 33, 34, 35,
        36, 37, 38, 39, 40, 41, 42, 43, 44,
         9, 10, 11, 12, 13, 14, 15, 16, 17,
        47, 50, 53, 46, 49, 52, 45, 48, 51
    ])
    rot_x = permutation([
        18, 19, 20, 21, 22, 23, 24, 25, 26,
        11, 14, 17, 10, 13, 16,  9, 12, 15,
        45, 46, 47, 48, 49, 50, 51, 52, 53,
        33, 30, 27, 34, 31, 28, 35, 32, 29,
         8,  7,  6,  5,  4,  3,  2,  1,  0,
        44, 43, 42, 41, 40, 39, 38, 37, 36
    ])
    rot_u = permutation([
         6,  3,  0,  7,  4,  1,  8,  5,  2,
        18, 19, 20, 12, 13, 14, 15, 16, 17,
        27, 28, 29, 21, 22, 23, 24, 25, 26,
        36, 37, 38, 30, 31, 32, 33, 34, 35,
         9, 10, 11, 39, 40, 41, 42, 43, 44,
        45, 46, 47, 48, 49, 50, 51, 52, 53
    ])
    rot_z = rot_x * rot_y * ~rot_x
    rot_r = ~rot_z * rot_u * rot_z
    rot_l = rot_z * rot_u * ~rot_z
    rot_d = rot_z**2 * rot_u * rot_z**2
    rot_f = rot_x * rot_u * ~rot_x
    rot_b = ~rot_x * rot_u * rot_x
    rot_m = ~rot_l * rot_r * rot_x
    rot_e = rot_u * ~rot_d * ~rot_y
    rot_s = ~rot_f * rot_b * rot_z
    
    moves = {
        'x' : rot_x,    'y' : rot_y,    'z' : rot_z,
        'xi': ~rot_x,   'yi': ~rot_y,   'zi': ~rot_z,
        'x2': rot_x**2, 'y2': rot_y**2, 'z2': rot_z**2,
        
        'u' : rot_u,    'r' : rot_r,    'l' : rot_l,
        'ui': ~rot_u,   'ri': ~rot_r,   'li': ~rot_l,
        'u2': rot_u**2, 'r2': rot_r**2, 'l2': rot_l**2,
        
        'd' : rot_d,    'f' : rot_f,    'b' : rot_b,
        'di': ~rot_d,   'fi': ~rot_f,   'bi': ~rot_b,
        'd2': rot_d**2, 'f2': rot_f**2, 'b2': rot_b**2,
        
        'm' : rot_m,    'e' : rot_e,    's' : rot_s,
        'mi': ~rot_m,   'ei': ~rot_e,   'si': ~rot_s,
        'm2': rot_m**2, 'e2': rot_e**2, 's2': rot_s**2
    }
