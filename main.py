#!/usr/bin/env python3

from cube import cube

def cube2bash(cube):
    tst = "  \x1b[48;5;{color}m    \x1b[0m"
    colors = {0: 196, 1: 26, 2: 15, 3: 28, 4: 226, 5: 208, -1:0} #RBWGYOK
    faces = cube.faces
    
    def buf_row(data):
        res = ''
        for d in data:
            res += tst.format(color=colors[d])
        return res
        
    mid = ''
    top = ''
    btm = ''
    for i in range(3):
        black = buf_row([-1]*3)
        tmp = black + buf_row(faces[0][i])
        top += f'{tmp}\n{tmp}\n\n'
        tmp = black + buf_row(faces[5][i])
        btm += f'{tmp}\n{tmp}\n\n'
    
    for i in range(3):
        tmp = ''
        for f in range(1, 5):
            tmp += buf_row(faces[f][i])
        mid += f'{tmp}\n{tmp}\n\n'
    return top+mid+btm

if __name__ == '__main__':
    rubik = cube()
    
    tmp = '.'
    while tmp:
        print(cube2bash(rubik))
        tmp = input('> ')
        
        moved = False
        for m in tmp.split(' '):
            if m not in cube.moves:
                continue
            rubik.rotate(m)
            moved = True
        
        if not moved and tmp:
            print(f'{tmp} not known! Available:')
            print('x, y, z: orientation')
            print('u, r, d, l, f, b, m, e, s: rotations')
            print('(plus their inverse and double variants, ex. xi r2)')
    
